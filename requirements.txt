# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.
Babel>=2.3.4 # BSD
eventlet!=0.18.3,>=0.18.2 # MIT
iso8601>=0.1.11 # MIT
jsonschema>=2.0.0,<3.0.0,!=2.5.0 # MIT
oslo.config>=3.14.0,!=3.18.0 # Apache-2.0
oslo.log>=3.11.0 # Apache-2.0
six>=1.9.0 # MIT
pbr>=1.8 # Apache-2.0
Jinja2>=2.8 # BSD License (3 clause)
stevedore>=1.17.1 # Apache-2.0
requests>=2.10.0,!=2.12.2 # Apache-2.0
urllib3>=1.15.1 # MIT
PyYAML>=3.10.0 # MIT
